#!/bin/bash

VERSION="0.1.0"
INSTALL_DIR="/etc/arkwrapper"
CONFIG="$INSTALL_DIR/arkwrapper.conf"
UPDATE_LOCK="/var/lock/arkwrapper_update.lock"

# Config
if [ -f "$CONFIG" ]; then
	source "$CONFIG"
else
	>&2 echo "Configuration file missing! Exiting.."
	exit 1
fi
FROM="From: $FROM_NAME <$FROM_MAIL>"

###############
## Functions ##
###############

function checkUID
{
        if [[ $EUID -ne 0 ]]; then
                >&2 echo "This script option $1 must be run as root"
                exit 1
        fi
}

function getPlayers
{
	local result=$(su - steam -c "arkmanager rconcmd ListPlayers | grep ^[0-9] | wc -l")
	if ! [[ "$result" =~ "^[0-9]+$" ]]; then
		echo "$result"
	else
		echo "-1"
	fi
}

function checkUpdate
{
	local UPDATE=$(su - steam -c "arkmanager checkupdate")

	# 0 for update available
	# 1 for no updates available
	# -1 for error
	if [ "$(echo "$UPDATE" | head -n 5 | tail -n 1)" == "Your server is up to date!" ]; then
		echo "1"
        elif [ "$(echo "$UPDATE" | grep -q "Available" && echo $?)" == "0" ]; then
		echo "0"
	else
		echo "-1"
	fi
}

function getVersion
{
	echo $(curl -s http://arkdedicated.com/version)
}

function setServerState
{
	sleep 10
	i=0
	while [ 1 ]
	do
		rawStatus=$(su - steam -c "arkmanager status")
		running=$(echo "$rawStatus" | grep running | cut -c 34-36)
		listening=$(echo "$rawStatus" | grep listening | cut -c 36-38)

		if [ "$running" == "Yes" ] && [ "$listening" == "Yes" ]; then
			exit 0;
		elif [ "$i" -gt 600 ]; then
			echo "Server have been done for 10 minutes after update!\n\nServer running: $running\nServer listening: $listening" | mail -a "$FROM" -s "$SUBJECT" "$MAIL"
			exit 1
		fi

		sleep 5
		i=$((i + 5))
	done
}

function doUpgrade
{
	# Update lock file
	echo "$BASHPID" > "$UPDATE_LOCK"

	# Save world
	SAVE_WORLD=$(su - steam -c "arkmanager rconcmd saveworld")
	RETVAL=$?
	if [ $RETVAL != 0 ]; then
		echo "Saveworld failed! Exit code: $RETVAL. Output: $SAVE_WORLD"
	else
		echo "$SAVE_WORLD"
	fi

	# Do the update
	if [ "$UPDATE_MODS" = true ]; then
		su - steam -c "arkmanager update --safe --update-mods"
	else
		su - steam -c "arkmanager update --safe"
	fi

	rm "$INSTALL_DIR/update"
	rm "$UPDATE_LOCK"
}

function doAutoUpdate
{
	checkUID "--cron-update"

	# Check if already updating and exit if true
	if [ -f "$UPDATE_LOCK" ]; then
		exit 0
	fi

	# Read the lastCheck time from file
	if [ -f "$INSTALL_DIR/lastcheck" ]; then
		lastCheck=$(cat $INSTALL_DIR/lastcheck)
	else
		touch "$INSTALL_DIR/lastcheck"
	fi

	# Check lastCheck value for null
	time=$(date +"%s")
	if [ -z $lastCheck ]; then
		lastCheck=$(expr "$time" - 3600)
	else
		lastCheck=$(expr "$time" - "$lastCheck")
	fi

	# Check when we last time checked the update, do not exit if update file exists
	if [ "$lastCheck" -lt "3580" ] && [ ! -f "$INSTALL_DIR/update" ]; then
		exit 0
	else
		echo "$(date +"%s")" > "$INSTALL_DIR/lastcheck"
	fi

	UPDATE=-1
	NOTIFY=0

	# If update file does not exists check update, if it exists we have update
	if [ ! -f "$INSTALL_DIR/update" ]; then
		UPDATE=$(checkUpdate)
	else
		UPDATE=0
	fi

	# No update available, exit
	if [ $UPDATE == 1 ]; then
		exit 0
	elif [ ! -f "$INSTALL_DIR/update" ] && [ $UPDATE == 0 ]; then
		touch "$INSTALL_DIR/update"
		echo "1" > "$INSTALL_DIR/update"
		NOTIFY=1
	elif [ $UPDATE == -1 ]; then
		echo "Error while checking update." | mail -a "$FROM" -s "$SUBJECT" "$MAIL"
		exit 1
	fi

	PLAYERS=$(getPlayers)
	CUR_VERSION=$(su - steam -c "arkmanager status" | grep "Server Name" | cut -d'(' -f2 | cut -c2- | rev | cut -c2- | rev)
	API_VERSION=$(getVersion)

	# Check version which arkdedicated reported
	if (( $(echo "$API_VERSION <= $CUR_VERSION" | bc -l) )); then
		VER_UPTODATE=false
	else
		VER_UPTODATE=true
	fi

	if [ "$PLAYERS" != 0 ] && [ "$NOTIFY" == 1 ]; then
		MAIL_MESSAGE=$(echo -e "Server update available.\n\nNumber of players in server $PLAYERS.")

		if [ "$PRE_DOWNLOAD" = true ]; then
			UPDATE_LOG=$(su - steam -c "arkmanager update --downloadonly")
			if [ $VER_UPTODATE = true ]; then
				MAIL_MESSAGE="$(echo -e "Server update available to version $API_VERSION.\n\nNumber of players in server $PLAYERS.")\n\n$UPDATE_LOG"
			else
				MAIL_MESSAGE="$(echo -e "Server update available but version number is unknown (server version: $CUR_VERSION, reported version: $API_VERSION).\n\nNumber of players in server $PLAYERS.")\n\n$UPDATE_LOG"
			fi
		fi
		if [ "$NOTIFY_SERVER" = true ] && [ "$PRE_DOWNLOAD" = true ]; then
			if [ $VER_UPTODATE = true ]; then
				su - steam -c "arkmanager rconcmd \"ServerChat ARK server update available to version $API_VERSION. When no one is connected the server will be updated.\"" &>/dev/null
			else
				su - steam -c "arkmanager rconcmd \"ServerChat ARK server update available but we were unable to get it's version. When no one is connected the server will be updated.\"" &>/dev/null
			fi
		fi

		echo -e "$MAIL_MESSAGE" | mail -a "$FROM" -s "$SUBJECT" "$MAIL"

		exit 0
	elif [ "$PLAYERS" == 0 ]; then
		UPDATE_LOG=$(doUpgrade 2>&1 | sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[mGK]//g" | grep -v '.*ark.*OK.*')

		# Send mail that update have been performed
		if [ $VER_UPTODATE = true ]; then
			printf "%s\n\n%s" "Server updated to $API_VERSION!" "$UPDATE_LOG" | mail -a "$FROM" -s "$SUBJECT" "$MAIL"
		else
			printf "%s\n\n%s" "Server updated but version number unknown (server version $CUR_VERSION, reported version: $API_VERSION)!" "$UPDATE_LOG" | mail -a "$FROM" -s "$SUBJECT" "$MAIL"
		fi
		# Start monitoring server
		setServerState &
		return 0
	fi
}

function doUpdate
{
	checkUID "--update"

	# Check if already updating and exit if true
	if [ -f "$UPDATE_LOCK" ]; then
		>&2 echo "The server is already being updated. Exiting.."
		exit 1
	fi

	echo "Querying Steam database for latest version..."

	UPDATE=-1

	# If update file does not exists check update, else update available
	if [ ! -f "$INSTALL_DIR/update" ]; then
		UPDATE=$(checkUpdate)
	else
		UPDATE=0
	fi

	PLAYERS=$(getPlayers)

	if [ $UPDATE == 1 ]; then
		echo " Server is up to date."
		exit 0
	elif [ $UPDATE == 0 ]; then
		touch "$INSTALL_DIR/update"
		echo "1" > "$INSTALL_DIR/update"
		echo " Number of players connected: $PLAYERS"
		echo ""
	else
		echo "Error while checking update!"
		exit 1
	fi

	echo -n "Perform update? "
	if [ "$PLAYERS" -eq 0 ]; then
		echo -n "[Y/n]"
		read -p " " -n 4 -r
		REPLY="$REPLY\n"
		if [ "$REPLY" == '\n' ]; then
			REPLY='Y'
		fi
	else
		echo -n "[y/N]"
		read -p " " -n4 -r
		REPLY="$REPLY\n"
		if [ "$REPLY" == '\n' ]; then
			REPLY="No"
		fi
	fi

	case "$REPLY" in
		Y*|y*)
			doUpgrade
			exit 0
			;;
		*)
			touch "$INSTALL_DIR/update"
			echo "1" > "$INSTALL_DIR/update"
			echo "No update performed"
			exit 0
			;;
	esac
}

function forceUpdate
{
	checkUID "--force-update"
	if [ -f "$INSTALL_DIR/update" ]; then
		rm "$INSTALL_DIR/update"
	fi
	if [ -f "$INSTALL_DIR/lastcheck" ]; then
		rm "$INSTALL_DIR/lastcheck"
	fi
}

function chatLogger
{
	checkUID "--chatlog"

	if [ -z "$CHAT_INTERVAL" ] || [ "$CHAT_INTERVAL" -lt 1 ]; then
		CHAT_INTERVAL=10
	fi

	while [ 1 ]; do
		CHAT=$(su - steam -c "arkmanager rconcmd GetChat" | sed '/^\s*$/d')
		TIME=$(date -d "now $TIME_SKEW hour" +"$TIME_FORMAT")

		if [ "$(echo "$CHAT" | grep ': ')" ]; then
			while read -r message; do
				printf "%s\n" "$TIME | $message" >> "$INSTALL_DIR/chat.log"
			done <<< "$CHAT"
			CHATLOG=$(cat $INSTALL_DIR/chat.log)
			echo "$CHATLOG" | tail -n 500 > "$INSTALL_DIR/chat.log"

			# Loop over enabled plugins
			for plugin in "$INSTALL_DIR"/plugins-enabled/*; do
				$plugin "$CHAT" "$TIME" "$FROM" "$MAIL"
			done
		fi

		sleep "$CHAT_INTERVAL"
	done
}

function chatLog
{
	if [ -f "$INSTALL_DIR/chat.log" ]; then
		cat "$INSTALL_DIR/chat.log"
	else
		echo "No log file."
	fi

	exit 0
}

function displayHelp
{
	echo "Usage: arkwrapper [OPTION]"
	echo "Saveworld is done before update via rcon and actual update performed with arkmanager option \"--safe\""
	echo ""
	echo " --update		Check update and ask if to perform update"
	echo " --cron-update		Use with cron to automate server update. See man arkwrapper(1)"
	echo " --force-update		Force script to query steam for available updates"
	echo " --chatlog		Monitors and logs the chat to disk, supports plugins"
	echo " --get-chat		Outputs the chat log from the chat.log"
	echo " --version                Display version and exit"
	echo " -H, --help		Display this help and exit"

	exit 0
}

###############
#### Main #####
###############

if [ "$#" -lt 1 ]; then
	>&2 displayHelp
	exit 1
elif [ "$#" -gt 2 ]; then
	echo "arkwrapper: Too many arguments"
	exit 1
fi

case "$1" in
	--version)
		echo "arkwrapper: $VERSION"
		exit 0
		;;
        -H|--help|'')
                displayHelp
                ;;
esac

# Check server tools version
ARKST_VER=$(grep '^arkstTag=' /usr/local/bin/arkmanager | cut -d'=' -f2 | cut -c3- | rev | cut -c2- | rev)
if [ -z "$ARKST_VER" ]; then
	echo "Could not determine ARK Server Tools version! Exiting.."
	exit 1
fi
ARKST_VER_MAJOR=$(echo "$ARKST_VER" | cut -d'.' -f1)
ARKST_VER_MINOR=$(echo "$ARKST_VER" | cut -d'.' -f2)
ARKST_VER_PATCH=$(echo "$ARKST_VER" | cut -d'.' -f3)
REQUIRED_ARKST_VER_MAJOR=1
REQUIRED_ARKST_VER_MINOR=5
ARKST_NOT_SUPPORTED=1

if [ "$ARKST_VER_MAJOR" -ge "$REQUIRED_ARKST_VER_MAJOR" ]; then
	if [ "$ARKST_VER_MINOR" -ge "$REQUIRED_ARKST_VER_MINOR" ]; then
		ARKST_NOT_SUPPORTED=0
	fi
fi

if [ $ARKST_NOT_SUPPORTED -gt 0 ]; then
	echo "Your ARK Server Tools (v$ARKST_VER) is not supported, versions from ${REQUIRED_ARKST_VER_MAJOR}.${REQUIRED_ARKST_VER_MINOR} upwards are supported"
	exit 1
fi

case "$1" in
	--update)
		if [ "$2" == "--force-update" ]; then
			forceUpdate
		fi
		doUpdate
		;;
	--cron-update)
		if [ "$2" == "--force-update" ]; then
			forceUpdate
		fi
		doAutoUpdate
		;;
	--update-state)
		setServerState
		;;
	--chatlog)
		chatLogger
		;;
	--get-chat)
		chatLog
		;;
	*)
		>&2 echo "arkwrapper: invalid option -- '$1'"
		exit 1
		;;
esac

exit 0
