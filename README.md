# arkwrapper

Script that relies to arkmanager to check ARK: Survival Evolved server updates every hour. If found available server update, updates server when there is no players connected to server. Updates and when server updated, mail is sent to email to you.

## Pre-requisites

I assume you have set up:

arkmanager and it's dependencies.

Your mailutils so you can send emails.

## Requirements

- [ark-server-tools](https://github.com/FezVrasta/ark-server-tools) v1.6.x

- mailutils (mail command)

## Install arkwrapper

Clone repository

Move cloned ``arkwrapper`` directory to ``/etc``

Move the ``arkwrapper`` script from ``arkwrapper`` directory to ``/usr/local/bin``

Put ``*/5 * * * * /usr/local/bin/arkwrapper --cron-update`` to your crontab

And you're done.

## Configuration

Configuration is in ``arkwrapper.conf``

`MAIL`, your email address where you would like to receive emails.

`FROM_NAME`, is the sender name.

`FROM_NAME`, email address where the emails "looks" to be coming from.

## Plugins

Available plugins are in ``plugins-available`` directory

To enable plugin, create symlink in ``plugins-enabled`` directory to point a plugin in ``plugins-available`` directory

Plugin configurations are stored in ``confs`` directory

## Commands

To get a complete list of the script commands you can run `arkwrapper --help` or `arkmanager -H`.

## Thanks
Big thanks to ark-server-tools maintainers
